/*
 * Copyright 2017-2018 Lauri Himanen, Fawzi Mohamed
 * 
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package eu.nomad_lab.normalizers

import eu.{ nomad_lab => lab }
import eu.nomad_lab.DefaultPythonInterpreter
import org.{ json4s => jn }
import scala.collection.breakOut
import eu.nomad_lab.normalize.ExternalNormalizerGenerator
import eu.nomad_lab.normalize.Normalizer
import eu.nomad_lab.meta
import eu.nomad_lab.query
import eu.nomad_lab.resolve._
import eu.nomad_lab.h5.EmitJsonVisitor
import eu.nomad_lab.h5.H5EagerScanner
import eu.nomad_lab.h5.SectionH5
import eu.nomad_lab.parsers.ExternalParserWrapper
import eu.nomad_lab.JsonUtils
import scala.collection.mutable.StringBuilder

object SymmetryNormalizer extends ExternalNormalizerGenerator(
  name = "SymmetryNormalizer",
  info = jn.JObject(
    ("name" -> jn.JString("SymmetryNormalizer")) ::
      ("parserId" -> jn.JString("SymmetryNormalizer" + lab.SymmetryVersionInfo.version)) ::
      ("versionInfo" -> jn.JObject(
        ("nomadCoreVersion" -> jn.JObject(lab.NomadCoreVersionInfo.toMap.map {
          case (k, v) => k -> jn.JString(v.toString)
        }(breakOut): List[(String, jn.JString)])) ::
          (lab.SymmetryVersionInfo.toMap.map {
            case (key, value) =>
              (key -> jn.JString(value.toString))
          }(breakOut): List[(String, jn.JString)])
      )) :: Nil
  ),
  context = "calculation_context",
  filter = query.CompiledQuery(query.QueryExpression("section_system and configuration_periodic_dimensions and atom_positions and atom_labels and simulation_cell"), meta.KnownMetaInfoEnvs.publicMeta),
  cmd = Seq(DefaultPythonInterpreter.pythonExe(), "${envDir}/normalizers/symmetry/normalizer/normalizer-symmetry/symmetry_analysis.py",
    "${contextUri}", "${archivePath}"),
  resList = Seq(
    "normalizer-symmetry/symmetry_analysis.py",
    "normalizer-symmetry/setup_paths.py",
    "nomad_meta_info/public.nomadmetainfo.json",
    "nomad_meta_info/common.nomadmetainfo.json",
    "nomad_meta_info/meta_types.nomadmetainfo.json",
    "nomad_meta_info/stats.nomadmetainfo.json"
  ) ++ DefaultPythonInterpreter.commonFiles(),
  dirMap = Map(
    "normalizer-symmetry" -> "normalizers/symmetry/normalizer/normalizer-symmetry",
    "nomad_meta_info" -> "nomad-meta-info/meta_info/nomad_meta_info",
    "python" -> "python-common/common/python/nomadcore"
  ) ++ DefaultPythonInterpreter.commonDirMapping(),
  metaInfoEnv = lab.meta.KnownMetaInfoEnvs.all

) {

  override def stdInHandler(context: ResolvedRef)(wrapper: ExternalParserWrapper)(pIn: java.io.OutputStream): Unit = {
    val out: java.io.Writer = new java.io.BufferedWriter(new java.io.OutputStreamWriter(pIn));
    val trace = Normalizer.trace
    val stringBuilder = if (trace)
      new StringBuilder
    else
      null
    def writeOut(s: String): Unit = {
      out.write(s)
      if (trace) stringBuilder ++= s
    }
    def flush(): Unit = {
      if (trace) {
        logger.info(stringBuilder.result())
        stringBuilder.clear()
      }
    }
    writeOut("[")
    var isFirst = true
    try {
      context match {
        case Calculation(archiveSet, c) =>

          def outputSysSection(sysSection: SectionH5): Unit = {
            /**
             * This function will output the given section to the python part
             */
            if (!isFirst)
              writeOut(",")
            else
              isFirst = false
            val visitor = new EmitJsonVisitor(
              writeOut = writeOut
            )
            val scanner = new H5EagerScanner
            scanner.scanResolvedRef(Section(archiveSet, sysSection), visitor)
            flush()
          }

          val sysTable = c.sectionTable(Seq("section_run", "section_system"))
          val sccTable = c.sectionTable(Seq("section_run", "section_single_configuration_calculation"))
          val fsTable = c.sectionTable(Seq("section_run", "section_frame_sequence"))

          // The symmetry information is calculated only for systems that are
          // linked to a section_single_configuration_calculation. Also if
          // there are multiple sccs, the symmetry information is calculated
          // only for the last frame belonging to a frame-sequence.
          val iSys: Seq[Long] = sccTable.lengthL match {
            case 0L =>
              Seq()
            case 1L =>
              sccTable(0).maybeValue("single_configuration_calculation_to_system_ref").map(_.longValue).toSeq
            case nSingleConfCalc =>
              if (!fsTable.isEmpty) {
                // use frame sequence
                val framesRefs = fsTable.subValueTable("frame_sequence_to_frames_ref")
                val sccIds: Seq[Long] = if (!framesRefs.isEmpty) {
                  Seq()
                } else {
                  val frames = framesRefs(0).arrayValue()
                  Seq(frames.getLong(0)) ++ {
                    val lFrames = if (framesRefs.lengthL > 1)
                      framesRefs(framesRefs.lengthL - 1).arrayValue()
                    else
                      frames
                    if (lFrames.getSize > 2)
                      Seq(
                        lFrames.getLong((frames.getSize() - 2).toInt),
                        lFrames.getLong((frames.getSize() - 1).toInt)
                      )
                    else
                      Seq(lFrames.getLong((frames.getSize() - 1).toInt))
                  }
                }
                sccIds.flatMap { f =>
                  sysTable(f).maybeValue("single_configuration_calculation_to_system_ref").map(_.longValue)
                }
              } else {
                Seq()
              }
          }
          val iSysAll: Seq[Long] = if (iSys.isEmpty) {
            sysTable.lengthL match {
              case 0L => Seq()
              case 1L => Seq(0)
              case 2L => Seq(0, 1)
              case n => Seq(0, n - 2, n - 1)
            }
          } else {
            iSys
          }
          for (i <- iSysAll)
            outputSysSection(sysTable(i))
          writeOut("]")
          flush()
        case r =>
          throw new Exception(s"SymmetryNormalizer expected a calculation as context, but got $r")
      }
    } finally {
      out.close()
      pIn.close()
      wrapper.sendStatus = ExternalParserWrapper.SendStatus.Finished
    }
  }
}

