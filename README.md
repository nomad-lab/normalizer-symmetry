# Symmetry Normalizer

This is the Normalizer that adds symmetry information for crystal systems.

It is part of the [NOMAD Laboratory](http://nomad-lab.eu).
The official version lives at

    git@gitlab.mpcdf.mpg.de:nomad-lab/normalizer-symmetry.git

you can browse it at

    https://gitlab.mpcdf.mpg.de/nomad-lab/normalizer-symmetry

It relies on having the nomad-meta-info and the python common repositories one level higher.
The simplest way to have this is to check out nomad-lab-base recursively:

    git clone --recursive git@gitlab.mpcdf.mpg.de:nomad-lab/nomad-lab-base.git

then this will be in normalizers/symmetry.
